import pytest
from db_utils import DBSessionProvider


@pytest.fixture(scope="session")
def db_session():
    """
    db session fixture
    :return:
    """

    db_session = DBSessionProvider.get_db_session()
    db_session.begin_nested()

    yield db_session

    # executed at teardown
    db_session.rollback()
    db_session.close()
