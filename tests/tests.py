import unittest
from abc import ABCMeta
import pytest
from db_utils import engine, DBSessionProvider


@pytest.mark.usefixtures()
class BaseTest(unittest.TestCase):

    __metaclass__ = ABCMeta


    @classmethod
    def setUpClass(cls):

        # cls.conn = engine.connect()
        # cls.trans = cls.conn.begin()
        cls.db_session = DBSessionProvider.get_db_session()

    def setUp(self):
        self.db_session.begin_nested()

    def tearDown(self):
        """
        This function is called after ever test in a class to clear the data created by the test
        :return:
        """
        self.db_session.rollback()

    @classmethod
    def tearDownClass(cls):
        pass
