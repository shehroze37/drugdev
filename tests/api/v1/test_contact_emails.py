import names
from models import Contact, Email


def test_contact_add(db_session):

    """
    test creation of contact and email
    :param db_session:
    :return:
    """
    first_name = names.get_first_name()
    last_name = names.get_last_name()
    username = "{}.{}".format(first_name, last_name)

    emails = ["{}.{}@gmail.com".format(first_name, last_name)]

    contact = Contact(firstname=first_name, lastname=last_name, username=username)
    for email in emails:
        em = Email(email=email)
        contact.emails.append(em)

    db_session.add(contact)
    db_session.flush()


    contact_ob = db_session.query(Contact).filter(Contact.username == username).one_or_none()
    assert contact_ob is not None

    # validate emails
    for email in contact_ob.emails:
        assert email.email in emails
