import unittest
from tests.tests import BaseTest
import pytest
import json
import names
from models import Contact, Email

API_PREFIX = '/api/v1/contacts/'


class TestContactsAPI(BaseTest, unittest.TestCase):


    @pytest.fixture(autouse=True)
    def app_client(
        self,
        application_client,
        header,
        db_session
    ):

        """
        Fixture to set up objects for use between all tests
        :param application_client:
        :param header:
        :return: None
        """

        self.application_client = application_client
        self.header = header
        self.api_endpoint = '/api/v1/contacts/'
        self.db_session = db_session

    def test_contact_add(self):
        """
        test that contact does not exist with the details and then test the creation
        of a new one
        :param application_client:
        :param header:
        :return:
        """
        first_name = names.get_first_name()
        last_name = names.get_last_name()
        username = "{}.{}".format(first_name, last_name)

        data = [
            {"firstname": first_name, "lastname": last_name, "username": username,
             "emails": ["{}.{}@gmail.com".format(first_name, last_name)]}
        ]

        res = self.application_client.get(
            "{}{}".format(API_PREFIX, username),
            headers=self.header
        )
        assert res.status_code == 404

        res = self.application_client.post(
            "{}".format(API_PREFIX),
            headers=self.header,
            data=json.dumps(data)
        )

        response_data = json.loads(res.data)
        assert res.status_code == 201


    def test_contact_delete(self):

        """
        create new contact and test it's deletion
        :param application_client:
        :param header:
        :return:
        """
        first_name = names.get_first_name()
        last_name = names.get_last_name()
        username = "{}.{}".format(first_name, last_name)

        data = [
            {"firstname": first_name, "lastname": last_name, "username": username, "emails":
                ["{}.{}@gmail.com".format(first_name, last_name)]
             }
        ]

        contact = Contact(firstname=first_name, lastname=last_name, username=username)
        email = Email(email="{}.{}@gmail.com".format(first_name, last_name))
        contact.emails.append(email)

        self.db_session.add(contact)
        self.db_session.flush()

        res = self.application_client.delete(
            "{}{}".format(API_PREFIX, username),
            headers=self.header
        )

        response_data = json.loads(res.data)
        assert res.status_code == 200
        assert response_data['message'] == 'Contact deleted'


    def test_contact_update(self):

        first_name = names.get_first_name()
        last_name = names.get_last_name()
        username = "{}.{}".format(first_name, last_name)

        contact = Contact(firstname=first_name, lastname=last_name, username=username)
        email = Email(email="{}.{}@gmail.com".format(first_name, last_name))
        contact.emails.append(email)

        self.db_session.add(contact)
        self.db_session.flush()

        new_first_name = names.get_first_name()
        data = [
            {"firstname": new_first_name, "lastname": last_name, "username": username,
             "emails": ["{}.{}@gmail.com".format(new_first_name, last_name)]}
        ]

        res = self.application_client.put(
            "{}{}".format(API_PREFIX, username),
            headers=self.header,
            data=json.dumps(data)
        )

        print("Creating and updating contact with firstname = {} and {}".format(first_name, new_first_name))

        response_data = json.loads(res.data)
        assert res.status_code == 200
        assert response_data['message'] == 'Contact Updated'

        contact = self.db_session.query(Contact).filter(Contact.username == username).one_or_none()
        assert contact is not None
        assert contact.firstname == new_first_name

    
    def test_contact_update_invalid_username(self):

        first_name = names.get_first_name()
        last_name = names.get_last_name()
        username = "{}.{}".format(first_name, last_name)

        data = [
            {"firstname": first_name, "lastname": last_name, "username": username,
             "emails": ["{}.{}@gmail.com".format(first_name, last_name)]}
        ]

        res = self.application_client.put(
            "{}{}".format(API_PREFIX, username),
            headers=self.header,
            data=json.dumps(data)
        )

        response_data = json.loads(res.data)
        assert res.status_code == 404


    def test_get_contacts_all(self):

        """
        create a number of contacts and retrieve all
        :param application_client:
        :param header:
        :return:
        """
        contacts = []
        contact_emails = []
        for i in range(4):

            first_name = names.get_first_name()
            last_name = names.get_last_name()
            username = "{}.{}".format(first_name, last_name)

            data = [
                {"firstname": first_name, "lastname": last_name, "username": username,
                 "emails": ["{}.{}@gmail.com".format(first_name, last_name)]}
            ]
            contact = Contact()
            contact.firstname = first_name
            contact.lastname = last_name
            contact.username = username

            for email in data[0]['emails']:
                contact_emails.append(email)
                contact.emails.append(Email(email=email))
            contacts.append(data[0])

            self.db_session.add(contact)
            self.db_session.flush()

        res = self.application_client.get(
            "{}".format(API_PREFIX),
            headers=self.header
        )
        assert res.status_code == 200
        response_data = json.loads(res.data)
        for contact in response_data:
            for email in contact['emails']:
                assert email in contact_emails

        assert len(response_data) == len(contacts)

