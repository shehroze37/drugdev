import pytest
from application import create_application


@pytest.fixture(scope="session")
def application_client():

    application = create_application()
    application.config['TESTING'] = True
    client = application.test_client()
    return client

@pytest.fixture(scope="session")
def header():

    header = {
        "Content-Type": "application/json"
    }
    return header