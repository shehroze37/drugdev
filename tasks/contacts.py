from application import init_celery
from celery.utils.log import get_task_logger
from config import ProductionConfig
logger = get_task_logger(__name__)
import names
from db_utils import DBSessionProvider
from models import Contact, Email

db_session = DBSessionProvider.get_db_session()

#celery = init_celery()
from celery_app import  celery
config = ProductionConfig()

header = {
        "Content-Type": "application/json"
}

class SqlAlchemyTask(celery.Task):
    """An abstract Celery Task that ensures that the connection the the
    database is closed on task completion"""
    abstract = True

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        db_session.remove()


@celery.task()
def delete_contact(username):

    logger.info("Deleting contact {}".format(username))

    db_session.query(Contact).filter(Contact.username == username).delete()
    db_session.commit()


@celery.task(base=SqlAlchemyTask)
def contact_creation_deletion():

    first_name = names.get_first_name()
    last_name = names.get_last_name()
    username = "{}.{}".format(first_name, last_name)
    email = "{}@gmail.com".format(username)

    contact = Contact(firstname=first_name, lastname=last_name, username=username)
    email_ob = Email(email=email)
    contact.emails.append(email_ob)

    db_session.add(contact)
    db_session.commit()
    logger.info("Contact added with username {}".format(username))

    delete_contact.apply_async(kwargs={'username': username}, countdown=60)


