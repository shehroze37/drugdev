
from celery.schedules import crontab
from datetime import timedelta


CELERY_IMPORTS = ('tasks.contacts')
CELERY_TASK_RESULT_EXPIRES = 30
CELERY_TIMEZONE = 'UTC'

CELERY_ACCEPT_CONTENT = ['json', 'msgpack', 'yaml']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
#CELERYD_POOL = "celery.concurrency.threads.TaskPool"



CELERYBEAT_SCHEDULE = {
    'test-celery': {
        'task': 'tasks.contacts.contact_creation_deletion',
        'schedule': timedelta(seconds=15),
    }
}
