from flask_sqlalchemy import SQLAlchemy
import os

project_dir = os.path.dirname(os.path.abspath(__file__))
development_database_file = "sqlite:///{}".format(os.path.join(project_dir, "drugdev.db"))

db = SQLAlchemy()


class Contact(db.Model):
    """Basic contact model
    """

    __tablename__ = 'contacts'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    firstname = db.Column(db.String(80), unique=False, nullable=False)
    lastname = db.Column(db.String(80), unique=False, nullable=False)

    emails = db.relationship('Email', backref='contact', passive_deletes=True, lazy=True)

    def __init__(self, **kwargs):
        super(Contact, self).__init__(**kwargs)

    def __repr__(self):
        return "<User %s>" % self.username


class Email(db.Model):

    __tablename__ = 'emails'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), nullable=False)
    contact_id = db.Column(db.Integer, db.ForeignKey('contacts.id', ondelete='cascade'), nullable=False)
