from flask import Flask, request, Response, jsonify
import json
import os
from celery import Celery

def configure_extensions(app, cli=False):
    """configure flask extensions
    """
    db.init_app(app)
    migrate.init_app(app, db)


def init_celery(app=None):

    app = create_application()
    celery = Celery()

    celery.conf.broker_url = app.config['CELERY_BROKER_URL']
    celery.conf.result_backend = app.config['CELERY_RESULT_BACKEND']

    celery.conf.update(app.config)
    import celery_config
    celery.config_from_object(celery_config)

    class ContextTask(celery.Task):
        """Make celery tasks work with Flask app context"""
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


def create_application():

    # main flask application
    application = Flask(__name__)

    from config import DevelopmentConfig, TestingConfig

    env = os.getenv('FLASK_ENVIRONMENT', 'debug')

    if env.lower() == 'testing':
        application.config.from_object(TestingConfig())
    else:
        application.config.from_object(DevelopmentConfig())

    from models import db
    db.init_app(application)

    # registering blueprints
    from v1 import contacts
    application.register_blueprint(contacts.blueprint, url_prefix='/api/v1/contacts')

    @application.route("/")
    def hello():

        message = {
            'status': 200,
            'message': 'API V1.0 IS LIVE ',
        }

        response = Response(json.dumps(message), status=200, mimetype='application/json')
        return response

    @application.errorhandler(404)
    def not_found(error=None):
        message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
        }
        resp = jsonify(message)
        resp.status_code = 404

        return resp

    return application

if __name__ == "__main__":

    application = create_application()
    application.run(host=application.config['HOST'], port=application.config['PORT'], threaded=True)
