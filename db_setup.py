from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os
from db_utils import engine

project_dir = os.path.dirname(os.path.abspath(__file__))
development_database_file = "sqlite:///{}".format(os.path.join(project_dir, "drugdev.db"))


#engine = create_engine(development_database_file, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    Base.metadata.create_all(bind=engine)

if __name__ == "__main__":
    print("Initializing Database")
    init_db()