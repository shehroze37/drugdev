from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import sys

import os

from application import create_application

app = create_application()

project_dir = os.path.dirname(os.path.abspath(__file__))
development_database_file = "sqlite:///{}".format(os.path.join(project_dir, "drugdev.db"))


app.config['SQLALCHEMY_DATABASE_URI'] = development_database_file
app.config['SQLALCHEMY_BINDS'] = {'test_db': "sqlite:///{}".format(os.path.join(project_dir, "drugdev_test.db"))}

from models import *
db.init_app(app)

migrate = Migrate()
migrate.init_app(app, db, render_as_batch=True)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
