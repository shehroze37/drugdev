import os

project_dir = os.path.dirname(os.path.abspath(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    DATABASE_URI = 'sqlite:///:memory:'

class ProductionConfig(Config):

    HOST = "0.0.0.0"
    PORT = 5000

class DevelopmentConfig(Config):


    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(project_dir, "drugdev.db"))
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    HOST = "0.0.0.0"
    PORT = 5000
    USE_CELERY = True
    SECRET_KEY = "changeme"

    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

    SQLALCHEMY_BINDS = {
        'test_db': "sqlite:///{}".format(os.path.join(project_dir, "drugdev_test.db"))
    }


class TestingConfig(Config):
    TESTING = True

    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(project_dir, "drugdev_test.db"))
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    HOST = "0.0.0.0"
    PORT = 5000
    USE_CELERY = True
    SECRET_KEY = "changeme"

    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'


def get_config_file():

    env = os.getenv('FLASK_ENVIRONMENT', 'debug')

    env_configuration_files = {

        "testing": "pytest.ini",
        "testing_ci": "pytest_docker.ini",
        "production": "production.ini",
        "debug": "development.ini"

    }

    config_file = os.path.join(
        os.path.dirname(os.path.realpath('__file__')),
        env_configuration_files[env.lower()]
    )

    return config_file


def get_config():

    env = os.getenv('FLASK_ENVIRONMENT', 'debug')
    config_obj = None

    if env.lower() == 'testing':
        config_obj = TestingConfig()
    elif env.lower() == "production":
        config_obj = ProductionConfig()
    else:
        config_obj = DevelopmentConfig()

    return config_obj
