from flask import Blueprint
from flask_restful import Api

from v1.resources.contacts import ContactResource, ContactList


blueprint = Blueprint('contacts', __name__)
api = Api(blueprint)


api.add_resource(ContactResource, '/<string:username>')
api.add_resource(ContactList, '/')


