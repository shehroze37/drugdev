from flask_restful import Resource, reqparse
from flask import request, jsonify
from flask_marshmallow import Marshmallow
from marshmallow import fields
from models import Contact, Email
from db_utils import DBSessionProvider

ma = Marshmallow()


parser = reqparse.RequestParser()
db_session = DBSessionProvider.get_db_session()

class EmailSchema(ma.Schema):

    email = fields.Str()

    class Meta:
        fields = ('email', )

class ContactSchema(ma.Schema):

    username = fields.Str()
    firstname = fields.Str()
    lastname = fields.Str()
    emails = fields.List(fields.Nested(EmailSchema))

    class Meta:
        fields = ('username', 'firstname', 'lastname', 'emails')


class ContactResource(Resource):
    """Single object resource
    """

    def get_contact(self, username):

        contact = db_session.query(Contact).filter(Contact.username == username).one_or_none()
        return contact

    def get(self, username):

        contact = db_session.query(Contact).filter(Contact.username == username).one_or_none()
        if contact is None:

            message = {
                'status': 404,
                'errors': 'No contact exists with this username'
            }

            resp = jsonify(message)
            resp.status_code = 404
            return resp

        contact_schema = ContactSchema()
        contact_schema_json = contact_schema.dump(contact)

        return contact_schema_json.data, 200


    def put(self, username):

        contact = self.get_contact(username)
        if contact is None:

            # return error that no contact exists with this username
            message = {
                'status': 404,
                'errors': 'No contact exists with this username'
            }

            resp = jsonify(message)
            resp.status_code = 404
            return resp

        data = request.json[0]
        fields = ['username', 'firstname', 'lastname', 'emails']

        for field in fields:
            if field in data:

                # update emails list with these emails
                if field == 'emails':
                    for email in data['emails']:
                        email_ob = Email(email=email)
                        contact.emails.append(email_ob)
                else:
                    setattr(contact, field, data[field])

        db_session.commit()

        message = {
            'message': 'Contact Updated'
        }
        return jsonify(message)

    def delete(self, username):

        if self.get_contact(username) is not None:

            db_session.query(Contact).filter(Contact.username == username).delete()
            db_session.commit()

            return jsonify({"message": "Contact deleted"})

        else:

            message = {
                'status': 404,
                'errors': 'No contact exists with this username'
            }

            resp = jsonify(message)
            resp.status_code = 404
            return resp


class ContactList(Resource):

    """Creation and get_all
    """

    def get(self):

        contacts = db_session.query(Contact).all()
        contact_schema = ContactSchema(many=True)
        contact_schema_json = contact_schema.dump(contacts)

        # post process emails for clear output
        for contact in contact_schema_json.data:
            clean_email_list = []
            for email in contact['emails']:
                clean_email_list.append(email['email'])

            contact['emails'] = clean_email_list

        return contact_schema_json.data, 200


    def post(self):

        #schema = ContactSchema(many=True)
        #contacts, errors = schema.load(request.json)

        fields = ['username', 'firstname', 'lastname', 'emails']
        contact_data = request.json

        for c in contact_data:
            contact = Contact()
            for field in fields:
                if field in c:

                    if field == 'emails':
                        for email in c['emails']:
                            email = Email(email=email)
                            contact.emails.append(email)
                    else:
                        setattr(contact, field, c[field])

            db_session.add(contact)
            db_session.commit()

        return {"message": "Contacts Added"}, 201